# {{ cookiecutter.module_name }} conda recipe

Home: "{{ cookiecutter.module_home }}/{{ cookiecutter.module_name }}"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: {{ cookiecutter.summary }}
